<?php

namespace App\Repository;

use App\Entity\TShirtLocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TShirtLocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method TShirtLocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method TShirtLocation[]    findAll()
 * @method TShirtLocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TShirtLocationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TShirtLocation::class);
    }

    // /**
    //  * @return TShirtLocation[] Returns an array of TShirtLocation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TShirtLocation
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
