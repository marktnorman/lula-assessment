<?php

namespace App\Repository;

use App\Entity\TShirt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TShirt|null find($id, $lockMode = null, $lockVersion = null)
 * @method TShirt|null findOneBy(array $criteria, array $orderBy = null)
 * @method TShirt[]    findAll()
 * @method TShirt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TShirtRepository extends ServiceEntityRepository
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * TShirtRepository constructor.
     *
     * @param RegistryInterface $registry
     * @param LoggerInterface   $logger
     */
    public function __construct(RegistryInterface $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, TShirt::class);
        $this->logger = $logger;
    }
}
