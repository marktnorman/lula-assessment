<?php

namespace App\Repository;

use App\Entity\TShirtBox;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TShirtBox|null find($id, $lockMode = null, $lockVersion = null)
 * @method TShirtBox|null findOneBy(array $criteria, array $orderBy = null)
 * @method TShirtBox[]    findAll()
 * @method TShirtBox[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TShirtBoxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TShirtBox::class);
    }

    // /**
    //  * @return TShirtBox[] Returns an array of TShirtBox objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TShirtBox
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
