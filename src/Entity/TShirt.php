<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TShirtRepository")
 */
class TShirt
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $color;

    /**
     * @ORM\Column(type="string", length=5)
     */
    public $size;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $label;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    public $rFid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TShirtBox", inversedBy="tShirts")
     */
    public $box;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TShirtLocation", mappedBy="tshirt", cascade={"persist", "remove"})
     */
    private $tShirtLocation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getRFid(): ?string
    {
        return $this->rFid;
    }

    public function setRFid(string $rFid): self
    {
        $this->rFid = $rFid;

        return $this;
    }

    public function getBox(): ?TShirtBox
    {
        return $this->box;
    }

    public function setBox(?TShirtBox $box): self
    {
        $this->box = $box;

        return $this;
    }

    public function getTShirtLocation(): ?TShirtLocation
    {
        return $this->tShirtLocation;
    }

    public function setTShirtLocation(TShirtLocation $tShirtLocation): self
    {
        $this->tShirtLocation = $tShirtLocation;

        // set the owning side of the relation if necessary
        if ($this !== $tShirtLocation->getTshirt()) {
            $tShirtLocation->setTshirt($this);
        }

        return $this;
    }
}
