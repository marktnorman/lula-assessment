<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TShirtLocationRepository")
 */
class TShirtLocation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastLocation;

    /**
     * @ORM\Column(type="array")
     */
    private $pastLocation = [];

    /**
     * @ORM\Column(type="datetime")
     */
    private $CreatedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TShirt", inversedBy="tShirtLocation", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $tshirt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastLocation(): ?string
    {
        return $this->lastLocation;
    }

    public function setLastLocation(string $lastLocation): self
    {
        $this->lastLocation = $lastLocation;

        return $this;
    }

    public function getPastLocation(): ?array
    {
        return $this->pastLocation;
    }

    public function setPastLocation(array $pastLocation): self
    {
        $this->pastLocation = $pastLocation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeInterface $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    public function getTshirt(): ?TShirt
    {
        return $this->tshirt;
    }

    public function setTshirt(TShirt $tshirt): self
    {
        $this->tshirt = $tshirt;

        return $this;
    }
}
