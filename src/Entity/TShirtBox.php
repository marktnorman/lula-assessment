<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TShirtBoxRepository")
 */
class TShirtBox
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array")
     */
    private $tShirtRfid = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TShirt", mappedBy="box")
     */
    private $tShirts;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $boxRfid;

    public function __construct()
    {
        $this->tShirts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTShirtRfid(): ?array
    {
        return $this->tShirtRfid;
    }

    public function setTShirtRfid(array $tShirtRfid): self
    {
        $this->tShirtRfid = $tShirtRfid;

        return $this;
    }

    /**
     * @return Collection|TShirt[]
     */
    public function getTShirts(): Collection
    {
        return $this->tShirts;
    }

    public function addTShirt(TShirt $tShirt): self
    {
        if (!$this->tShirts->contains($tShirt)) {
            $this->tShirts[] = $tShirt;
            $tShirt->setBox($this);
        }

        return $this;
    }

    public function removeTShirt(TShirt $tShirt): self
    {
        if ($this->tShirts->contains($tShirt)) {
            $this->tShirts->removeElement($tShirt);
            // set the owning side to null (unless already changed)
            if ($tShirt->getBox() === $this) {
                $tShirt->setBox(null);
            }
        }

        return $this;
    }

    public function getBoxRfid(): ?string
    {
        return $this->boxRfid;
    }

    public function setBoxRfid(string $boxRfid): self
    {
        $this->boxRfid = $boxRfid;

        return $this;
    }
}
