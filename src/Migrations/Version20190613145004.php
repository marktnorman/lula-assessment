<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190613145004 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tshirt RENAME INDEX r_fid TO UNIQ_6CF6F57964E4997B');
        $this->addSql('ALTER TABLE tshirt_location ADD tshirt_id INT NOT NULL');
        $this->addSql('ALTER TABLE tshirt_location ADD CONSTRAINT FK_E34D63C974D452 FOREIGN KEY (tshirt_id) REFERENCES tshirt (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E34D63C974D452 ON tshirt_location (tshirt_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tshirt RENAME INDEX uniq_6cf6f57964e4997b TO r_fid');
        $this->addSql('ALTER TABLE tshirt_location DROP FOREIGN KEY FK_E34D63C974D452');
        $this->addSql('DROP INDEX UNIQ_E34D63C974D452 ON tshirt_location');
        $this->addSql('ALTER TABLE tshirt_location DROP tshirt_id');
    }
}
