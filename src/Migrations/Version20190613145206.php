<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190613145206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('yellow', 'm', 'billing_bong')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('red', 's', 'lizzard')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('black', 'l', 'ecko')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('white', 'm', 'adidas')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('purple', 'm', 'reebok')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('pink', 's', 'billing_bong')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('green', 'm', 'lizzard')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('orange', 's', 'adidas')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('grey', 'l', 'reebok')");
        $this->addSql("INSERT INTO tshirt (color, size, label) VALUES ('navy', 'l', 'ecko')");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
