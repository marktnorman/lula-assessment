<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190612182444 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tshirt ADD box_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tshirt ADD CONSTRAINT FK_6CF6F579D8177B3F FOREIGN KEY (box_id) REFERENCES tshirt_box (id)');
        $this->addSql('CREATE INDEX IDX_6CF6F579D8177B3F ON tshirt (box_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tshirt DROP FOREIGN KEY FK_6CF6F579D8177B3F');
        $this->addSql('DROP INDEX IDX_6CF6F579D8177B3F ON tshirt');
        $this->addSql('ALTER TABLE tshirt DROP box_id');
    }
}
