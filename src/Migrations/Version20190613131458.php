<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190613131458 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE tshirt ADD UNIQUE (r_fid);');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE tshirt DROP INDEX r_fid;');
    }
}
