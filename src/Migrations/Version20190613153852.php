<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190613153852 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $serializedArray = "'" . serialize(['Manufactured']) . "'";
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '1')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '2')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '3')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '4')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '5')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '6')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '7')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '8')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '9')");
        $this->addSql("INSERT INTO tshirt_location (last_location, past_location, created_at, tshirt_id) VALUES ('Manufactured', $serializedArray, NOW(), '10')");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
