<?php

namespace App\Controller;

use App\Entity\TShirt;
use App\Entity\TShirtBox;
use App\Entity\TShirtLocation;
use App\Repository\TShirtRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\EntityManagerInterface;

class TShirtController extends AbstractController
{

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function getTshirt(Request $request): Response
    {
        try {
            // Let try get our tshirt if exists
            $repository = $this->getDoctrine()->getRepository(TShirt::class);
            $tShirt     = $repository->findOneBy(['rFid' => $request->query->get('rfid')]);

            if (!$tShirt) {
                throw $this->createNotFoundException(
                    'No T-Shirts found for rFid - ' . $request->query->get('rfid')
                );
            }

            // Creating our response;
            $response = new JsonResponse($tShirt);
            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();

        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(
                [
                    'message' => $e->getMessage()
                ]
            );

            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function getTshirtLocationHistory(Request $request): Response
    {
        try {
            // Let try get our tshirt if exists
            $tShirtRepository = $this->getDoctrine()->getRepository(TShirt::class);
            $tShirt           = $tShirtRepository->findOneBy(['rFid' => $request->query->get('rfid')]);

            if (!$tShirt) {
                throw $this->createNotFoundException(
                    'No T-Shirts found for rFid - ' . $request->query->get('rfid')
                );
            }

            // Lets find out more information about this TShirt
            $tShirtLocationRepository = $this->getDoctrine()->getRepository(TShirtLocation::class);
            $tShirtLocation           = $tShirtLocationRepository->findOneBy(['tshirt' => $tShirt->getId()]);
            $tShirtLocationData       = [
                'id'             => $tShirtLocation->getId(),
                'last_location'  => $tShirtLocation->getLastLocation(),
                'past_locations' => $tShirtLocation->getPastLocation(),
                'created_at'     => $tShirtLocation->getCreatedAt(),
                'tshirt_id'      => $tShirtLocation->getTshirt()
            ];

            // Creating our response
            $response = new JsonResponse($tShirtLocationData);
            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();

        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(
                [
                    'message' => $e->getMessage()
                ]
            );

            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function registerBlankTshirt(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $requestBody = [];
        if ($content = $request->getContent()) {
            $requestBody = json_decode($content, true);
        }

        try {
            if (empty($requestBody['color']) || empty($requestBody['size']) || empty($requestBody['label'])) {
                throw $this->createNotFoundException(
                    'Bad request, please be sure to specify values for all required fields - ' . $requestBody['request']
                );
            }

            $tShirt = new TShirt();
            $tShirt->setColor($requestBody['color']);
            $tShirt->setSize($requestBody['size']);
            $tShirt->setLabel($requestBody['label']);
            $entityManager->persist($tShirt);
            $entityManager->flush();

            // Creating our response;
            $response = new JsonResponse($tShirt);
            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();

        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(
                [
                    'message' => $e->getMessage()
                ]
            );

            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();
        }
    }

    /**
     * @param         $rFid
     * @param Request $request
     *
     * @return Response
     */
    public function registerTshirt($rFid, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $requestBody = [];
        if ($content = $request->getContent()) {
            $requestBody = json_decode($content, true);
        }

        try {
            // Lets see if this tFid has been assigned
            $tShirtRepository = $this->getDoctrine()->getRepository(TShirt::class);
            $tShirt           = $tShirtRepository->findOneBy(['rFid' => $rFid]);

            if (empty($tShirt)) {
                // Lets find our first tshirt which doesn't have rFid sewned into it
                $availabletShirt = $tShirtRepository->findOneBy(['rFid' => null]);

                if (!$availabletShirt) {
                    throw $this->createNotFoundException(
                        'All T-Shirts have been assigned to a rFid for request - ' . $requestBody['request']
                    );
                }

                $availabletShirt->setRFid($rFid);
                $entityManager->persist($availabletShirt);

                // Now that we've sewned a rFid to a TShirt, lets update its last location
                $tShirtLocationRepository = $this->getDoctrine()->getRepository(TShirtLocation::class);
                $tShirtLocation           = $tShirtLocationRepository->find($availabletShirt->getId());

                // Set last location of TShirt
                $tShirtLocation->setLastLocation('RFid Scanned');
                $entityManager->persist($tShirtLocation);

                //Let get the past location values and add to it
                $pastLocations = $tShirtLocation->getPastLocation();

                // Lets set the new location along with its history
                array_push($pastLocations, 'RFid Scanned');
                $tShirtLocation->setPastLocation($pastLocations);
                $entityManager->persist($tShirtLocation);

                $entityManager->flush();

                // Creating our response;
                $response = new JsonResponse($availabletShirt);
                $response->setStatusCode(Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');

                $response->send();
            } else {
                // TShirt already exists with RFid
                throw $this->createNotFoundException(
                    'T-Shirt already has this rFid assigned to it - ' . $rFid . ' for request - ' . $requestBody['request']
                );
            }
        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(
                [
                    'message' => $e->getMessage()
                ]
            );

            $response->setStatusCode(Response::HTTP_FOUND);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();
        }
    }

    /**
     * @param         $rFid
     * @param Request $request
     *
     * @return Response
     */
    public function registerBox($rFid, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $requestBody = [];
        if ($content = $request->getContent()) {
            $requestBody = json_decode($content, true);
        }

        try {
            // Lets grab limit of 5 available tShirts to pack inside a box that has RFid
            $tShirtRepository = $this->getDoctrine()->getRepository(TShirt::class);
            $tShirts          = $tShirtRepository->findBy(['box' => null], null, 5);

            if (!empty($tShirts)) {
                $tShirtIds = [];

                // Lets build array values of tShirt ID's
                foreach ($tShirts as $tShirt) {
                    $tShirtIds[] = $tShirt->getId();
                }

                // Lets only utilise the values
                $tShirtIds = array_values($tShirtIds);

                // Lets see if this rFid has been assigned
                $tShirtBoxRepository = $this->getDoctrine()->getRepository(TShirtBox::class);
                $tShirtBox           = $tShirtBoxRepository->findOneBy(['boxRfid' => $rFid]);

                if (empty($tShirtBox)) {
                    $tShirtBox = new TShirtBox();
                    $tShirtBox->setTShirtRfid($tShirtIds);
                    $tShirtBox->setBoxRfid($rFid);
                    $entityManager->persist($tShirtBox);
                    $entityManager->flush();

                    // Lets go and update the tShirts with their respective box IDs
                    foreach ($tShirts as $tShirt) {
                        $tShirtEntity = $tShirtRepository->find($tShirt->getId());
                        $tShirtEntity->setBox($tShirtBox);

                        $getTshirtRfid = $tShirtEntity->getRFid();

                        // If the T shirt has empty RFID, lets set it
                        if (empty($getTshirtRfid)) {
                            $tShirtEntity->setRFid($this->generateRFid());
                        }

                        $entityManager->persist($tShirtEntity);
                    }

                    /// Flush after setting the shirts
                    $entityManager->flush();

                    $tShirtBoxData = [
                        'id'          => $tShirtBox->getId(),
                        't_shirt_ids' => $tShirtBox->getTShirtRfid(),
                        'box_rfid'    => $tShirtBox->getBoxRfid(),
                    ];

                    $response = new JsonResponse($tShirtBoxData);
                    $response->setStatusCode(Response::HTTP_OK);
                    $response->headers->set('Content-Type', 'application/json');

                    $response->send();

                } else {
                    // Box already exists with RFid
                    throw $this->createNotFoundException(
                        'Box already packed with this rFid assigned to it, please use the getBox request with rFid - ' . $rFid . ' for request - ' . $requestBody['request']
                    );
                }
            } else {
                // There are no more TShirt that are available to be packed inside a box
                throw $this->createNotFoundException(
                    'There are no more TShirt that are available to be packed inside a box with rFid - ' . $rFid . ' for request - ' . $requestBody['request']
                );
            }
        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(
                [
                    'message' => $e->getMessage()
                ]
            );

            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function getBox(Request $request): Response
    {
        try {
            // Let try get our box information
            $boxRepository = $this->getDoctrine()->getRepository(TShirtBox::class);
            $box           = $boxRepository->findOneBy(['boxRfid' => $request->query->get('rfid')]);

            if (!$box) {
                throw $this->createNotFoundException(
                    'No box found for rFid - ' . $request->query->get('rfid')
                );
            }

            // Lets find out more information about this TShirt
            $boxData = [
                'id'           => $box->getId(),
                't_shirt_rfid' => $box->getTShirtRfid(),
                'box_rfid'     => $box->getBoxRfid()
            ];

            // Creating our response
            $response = new JsonResponse($boxData);
            $response->setStatusCode(Response::HTTP_OK);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();

        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(
                [
                    'message' => $e->getMessage()
                ]
            );

            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $response->headers->set('Content-Type', 'application/json');

            $response->send();
        }
    }

    /**
     * Lets generate a unique rFid for T shirts,
     * one example would be when the box register function is called
     * and no rFid has been set to that respective TShirt
     *
     * @param int $length
     *
     * @return string
     */
    private function generateRFid($length = 18)
    {
        $characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $rFid             = '';

        for ($i = 0; $i < $length; $i++) {
            $rFid .= $characters[rand(0, $charactersLength - 1)];
        }

        return $rFid;
    }
}
