# Start with this base image
FROM debian:stretch-slim

MAINTAINER Mark Norman <info@marktnorman.com>

# Create a list of phpize dependencies
ENV PHPIZE_DEPS \
		autoconf \
		dpkg-dev \
		file \
		g++ \
		gcc \
		libc-dev \
		make \
		pkg-config \
		re2c

# Update the package lists from the package repositories and install these packages
RUN apt-get update && \
    apt-get install -y --allow-downgrades --no-install-recommends \
        $PHPIZE_DEPS \
        wget \
        apt-transport-https \
        lsb-release \
        ca-certificates \
        curl \
        nano && \
    rm -rf /var/lib/apt/lists/*

# Download php packages into our sources list
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list

# Again we update the package lists from the package repositories and install these packages
RUN apt-get update && \
    apt-get install -y --allow-downgrades \
        php7.2 \
        php7.2-apcu \
        php7.2-fpm \
        php7.2-mysql \
        php7.2-curl \
        php7.2-intl \
        php7.2-memcached \
        php7.2-dom \
        php7.2-bcmath \
        php7.2-zip \
        php7.2-mbstring \
        php7.2-gmp \
        php7.2-xdebug && \
        rm -rf /var/lib/apt/lists/*



#RUN apt-get install -y curl
# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

# Add our own config
ADD application.pool.conf /etc/php/7.2/fpm/pool.d/

# Change www-data user to match the host system UID
RUN usermod -u 1000 www-data

# Creating a tmpfs for php
RUN mkdir -p /run/php

# Run php
CMD ["php-fpm7.2", "-F"]

# Create our volume
VOLUME /data/application

# Specify the work directory
WORKDIR /data/application

# Expose port 9000 to other containers
EXPOSE 9000
