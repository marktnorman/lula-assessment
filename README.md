##Lula assessment

### Setup

This application requires docker to be running.

Docker container consist of the following -

`PHP` - With name lula-assessment_php_1

`MySQL` - With name lula-assessment_mysql_1

`NGINX` - With name lula-assessment_nginx_1

- cd into the project root

`cd ~/path-to-project/lula-assessment`

 - Run the initialization command from that directory

`make init`

The app will now be accessible at http://127.0.0.1:8080/

Please find the PAW request file holding all the available routes -

`lula_assessment_requests.paw`

### Extra

Clearing cache - 

`bin/console c:c`

###Questions -

1. Describe your solution in a few words (expect an experienced programmer reading it) and describe
   your choice of libraries and/or patterns.
2. How do you handle transactions?
3. What would be your preferred method of hosting the server?
4. How do you approach logging in Node?
5. What is an event loop in Node and how does it interact with asynchronous tasks?
6. How do you handle errors?
7. How do you prefer to receive configuration?

###Answers -

LULA assessment schema consisted of the following -

- Schema (3 tables, tShirt, tShirtBox and tShirtLocation, all relating to each other by utilizing FOREIGN KEYS, prefer this way rather than plonking all the information into one / two tables. cleaner / neater. Dummy content inserted upon initializing the application is the route I’ve gone.)

- tShirt method inserts 10 tShirts that a rFid can be scanned to and last seen location defined in the tShirtLocation table.

- getTshirt by rFid to display information about it.  

- Same goes for location history of a tShirt journey by rFid.

- Testing this application might cause you to run out of tShirts for assigning / manufacturing new ones. So I created a registerBlankTshirt function to insert one with required fields.  

- registerBox will check for 5 available tShirts that has not been packed yet, regardless of them having a rFid assigned. Box will be created, rFid assigned to it and all of it’s tShirt rFids, now here it’s currently utilizing the tShirts id, a migration can be created to update the index key relation and also update entities themselves. Unique rFids will be assigned to tShirts that are packed which does not have any attached to them.  

- getBox just displays information about the box, holding the tshirts and their last seen location and LONG TEXT array stored past locations.

- Generic generateRfid method created for the register box method.

- Error handling has been used across the methods and returning the relevant error code / message.  

In terms of Symfony, when instantiating the entity manager and persisting your object, flushing will act as the commit and rollback of a transaction. In basic terms, transactions are tech which allows you to initiate a transaction, build your object / query and then execute, if fails one has the ability to rollback. 

For this assessment I utilized Docker, I love docker. One can also utilize the built in PHP server by running - symfony server:start. For personal projects of mine, I utilize my own AWS stack, running Ubuntu / PHP / NGINX. I’ve come along way of hosting many WP sites that got hacked, as we all know how vulnerable so many of the WP plugins are. 

For logging purposes, Symfony comes with a default PSR logger, but my preferred has always been monologger. 

Well as for starters, I’m not familiar with NODE JS but from what I know, event loop is a single thread executing code but at the same time executing callback functions in a 5 step phase process. For Symfony, we have event dispatchers and event listeners, on Kernel request before hitting our controller, one can modify the request to the controllers need, kernel dispatch and inside controller, execute what you need to do and then the dispatch your object, which has listeners defined / attached to it, basically acting as middleware. 

Well errors should be logged in various levels, info, warning, debug, notice, critical, emergency and so forth. Handling errors inside on methods should always have, as we call it, defensiveness checks, never assume anything, try / catch exceptions, when needed to utilize values, make sure they’re present, not empty / isset etc. 

If I understand the question correctly, in terms sensitive configuration information, should never be committed, pulled from ENV or desired configuration file, but the configs should be hosted either on the NGINX server on live environment, defined as a variable that can be pulled into a config file on deploy. Another method is to store in the DB, encrypted and decrypted when needed.

##   Extra notes -

  All in all the application can be improved with utilizing monologger to log to relevant locations, upload to a logging service such as Cloudwatch.  Also more validation on the api requests coming through validating the methods and required fields, not returning 500, one should never return 500.
